import 'package:flutter/material.dart';
import './control.dart';

class Output extends StatefulWidget {
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<Output> {
  String msg = 'Saputra Cahyo';

  void _changeText() {
    setState(() {
      if (msg.startsWith('S')) {
        msg = 'Cahyo Saputra';
      } else if (msg.startsWith('M')) {
        msg = 'Saputra Cahyo';
      } else {
        msg = 'Muh Cahyo Saputra';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Nama Saya : ',
            style: new TextStyle(fontSize: 35.0),
          ),
          Control(msg),
          RaisedButton(
            child: Text(
              "Ubah Name",
              style: new TextStyle(color: Colors.yellowAccent, fontSize: 20.0),
            ),
            color: Theme.of(context).primaryColor,
            onPressed: _changeText,
          ),
        ],
      ),
    );
  }
}
