import 'package:flutter/material.dart';
import './output.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark, primarySwatch: Colors.deepOrange),
      debugShowCheckedModeBanner: false,
      title: 'UTS MOBILE APP 2019',
      home: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            new Icon(Icons.favorite),
            new Icon(Icons.search),
            new Icon(Icons.more_vert),
          ],
          leading: new Icon(Icons.home),
          title: Text('UTS MOBILE APP 2019'),
        ),
        body: Output(),
      ),
    );
  }
}
